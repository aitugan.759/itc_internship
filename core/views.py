from django.shortcuts import render
from django.views import View


class Homepage(View):
    def get(self, request):
        context = {'title': 'Главная страница',
                   'hello': 'Добро пожаловать!',
                   'info': 'Информация о приложении'
                   }
        return render(request=request, template_name="core/mainpage.html", context=context)
class Info(View):
    def get(self, request):
        context = {'title': 'Информация о проекте'}
        return render(request=request, template_name="core/info.html", context=context)
