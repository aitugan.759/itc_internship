def func_task1(day: int, month: int, year: int):
    '''Функция возврата даты по входным day, month, year'''
    days = [i for i in range(1, 32)]
    months = {1: 'января', 2: 'февраля', 3: 'марта', 4: 'апреля', 5: 'мая', 6: 'июня', 7: 'июля', 8: 'августа',
              9: 'сентября', 10: 'октября', 11: 'ноября', 12: 'декабря'}
    leap_year = year % 4 == 0 and day <= 29
    february = month == 2
    if day in days and month in months.keys():
        if february:
            if leap_year:
                print(f'{day} {months.get(month)} {year} года')
            else:
                raise ValueError(f'Несуществующая дата!')
        if not february:
            print(f'{day} {months.get(month)} {year} года')
    else:
        raise ValueError(f'Несуществующая дата!')


def func_task2(*names: tuple):
    '''Функция подсчета имен в кортеже'''
    result = {}
    for name in names:
        if name in result:
            result[name] += 1
        else:
            result[name] = 1
    return result


def func_task3(d: dict):
    '''Функция вывода ФИО по входящему словарю'''
    full_name = ""
    if 'last_name' in d:
        full_name += d['last_name']
    if 'first_name' in d:
        full_name += ' ' + d['first_name']
    if 'middle_name' in d:
        full_name += ' ' + d['middle_name']
    if full_name == '':
        if 'middle_name' in d:
            return d['middle_name']
        elif 'last_name' in d:
            return d['last_name']
        else:
            return 'Нет данных'
    else:
        return full_name


def func_task4(num: int):
    '''Функция, проверяющая входящее число - простое или составное'''
    divider = 2
    while num % divider != 0:
        divider += 1
    return divider == num

def func_task5(*args):
    '''Функция, возвращающая в порядке возрастания числа из входящего множества'''
    numbers =[]
    for i in args:
        if isinstance(i, int) or isinstance(i, float):
            if not isinstance(i, bool) and i not in numbers:
                numbers.append(i)
    return sorted(numbers)
